package br.com.senac.exercicio8;

import org.junit.Test;
import static org.junit.Assert.*;

public class TesteICMS {

    public TesteICMS() {
    }

    @Test
    public void calcularImpostoRJ() {

        CalculadoraICMS vendaRJ = new CalculadoraICMS();
        double result = vendaRJ.calcularICMSRJ("Bolsonaro", 100, "RJ");

        assertEquals(17, result, 0.01);

    }

    @Test
    public void calcularImpostoSP() {

        CalculadoraICMS vendaSP = new CalculadoraICMS();
        double result = vendaSP.calcularICMSSP("Jair", 100, "SP");

        assertEquals(18 , result, 0.01);

    }

    @Test
    public void calcularImpostoMA() {

        CalculadoraICMS vendaMA = new CalculadoraICMS();
        double result = vendaMA.calcularICMSMA("Messias", 100, "MA");

        assertEquals(12, result, 0.01);

    }

}
