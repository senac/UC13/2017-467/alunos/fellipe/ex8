package br.com.senac.exercicio8;

public class CalculadoraICMS extends Venda {

  

    public double calcularICMSRJ(String cliente, double produtoValor, String estado) {
        setCliente(cliente);
        setValorProduto(produtoValor);
        setEstado(estado);
        double resultado = getValorProduto() * ICMS_RJ;
        return resultado;

       
    }

    
    public double calcularICMSSP(String cliente, double produtoValor, String estado) {
        setCliente(cliente);
        setValorProduto(produtoValor);
        setEstado(estado);
        double resultado = getValorProduto() * ICMS_SP;
        return resultado;

       
    }
    
    public double calcularICMSMA(String cliente, double produtoValor, String estado) {
        setCliente(cliente);
        setValorProduto(produtoValor);
        setEstado(estado);
        double resultado = getValorProduto() * ICMS_MA;
        return resultado;

       
    }
    
}
