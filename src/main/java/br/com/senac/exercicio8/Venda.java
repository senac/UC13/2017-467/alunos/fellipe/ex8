package br.com.senac.exercicio8;

public class Venda {

    public final double ICMS_RJ = 0.17;
    public final double ICMS_SP = 0.18;
    public final double ICMS_MA = 0.12;

    private String cliente;
    private double valorProduto;
    private String estado;

    public Venda() {
    }

    public Venda(String cliente, double valorProduto, String estado) {
        this.cliente = cliente;
        this.valorProduto = valorProduto;
        this.estado = estado;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getValorProduto() {
        return valorProduto;
    }

    public void setValorProduto(double valorProduto) {
        this.valorProduto = valorProduto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    

}
